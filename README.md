# Laptop Ensemble

<img src="https://gitlab.cecs.anu.edu.au/u5905548/Laptop-Ensemble/raw/master/images/facebook-banner.png" alt="LENS banner"/>

## Techlauncher Reference
### Output
* [Gig Recordings](https://drive.google.com/drive/folders/1STXxFJikBo6ygI-8EpHWvc0Lx5i6HNv-)
* [Youtube Channel](https://www.youtube.com/channel/UCIU6SqIS02GJlnLOPqlwmpA)
* [Code & Patches](https://gitlab.cecs.anu.edu.au/u5905548/Laptop-Ensemble/tree/master/code)
* [Guides](https://drive.google.com/drive/u/0/folders/1Sqsv41CddVyzS5bqjQUCdlZUFSRWruti)

### Decision Making
* [Diaries](https://drive.google.com/drive/folders/13lp9J8VFwJHu8zEdHMKw9TEs_0cJXoQE)
* [Decision Tracker](https://docs.google.com/document/d/1joUkYfg-11r9ZbyNsYCGPaC4JVHGV8FxUEPRLlLs-EM/edit?usp=sharing)
* [Meeting Minutes](https://drive.google.com/drive/folders/1ZvZFeZxwE-PmV46EMIhKdh0mRHBKSPm1)
* [Reflections](https://drive.google.com/drive/folders/1NVS6o_0uITDxhtJR8yggZjlRRcxFSPB2)
* [Admin Documents](https://drive.google.com/drive/folders/1caBU1Pl0VtFtKHADIP6sQNDRfdQfXFJ6)

### Teamwork
* [Discord Chatlogs](https://drive.google.com/drive/folders/1vBfw8kKa-tG1eNGEaPPNZB67pZaudBMn)
* [Reflections](https://drive.google.com/drive/folders/1NVS6o_0uITDxhtJR8yggZjlRRcxFSPB2)
* [Individual & Group Goals](https://docs.google.com/document/d/1FInPGQuwrH_g_U16Dk3iW4fhYgByhniNLkC1AmRdzSw/)

### Communication (with client)
* [Gig Reflections](https://drive.google.com/drive/folders/10mOyoYd2sh8nOBbT7jiircJuVyZtjcRq)
* [Meeting Minutes](https://drive.google.com/drive/folders/1ZvZFeZxwE-PmV46EMIhKdh0mRHBKSPm1)

At least one of our clients were present at our rehersals so we were able to get feedback from them on a regular basis

### Reflection
* [Reflections](https://drive.google.com/drive/folders/1NVS6o_0uITDxhtJR8yggZjlRRcxFSPB2)
* [Diaries](https://drive.google.com/drive/folders/13lp9J8VFwJHu8zEdHMKw9TEs_0cJXoQE)  
   While all diaries should have some reflection, the week 10 diary contains a large amount of reflections on goals and the project as a whole.


## Table of Contents
* [Introduction](#introduction)
* [The Team](#team)
* [Gigs](#gigs)
* [Gig recordings](#gig-recordings)
* [Meeting minutes](#meeting-minutes)
* [Rehearsal](#rehearsal)
* [Diaries](#diaries)
* [Plan](#plan)
* [Links](#links)
* [Stakeholders](#stakeholders)
* [Team Bios](#team-bios)
* [Equipment / Tools](#equipment-tools)

## Introduction

### Vision
The laptop<sup><a name="laptop"></a>[1](#anchor-1)</sup> is a legit musical/visual instrument, and the [code/creativity/culture](https://cs.anu.edu.au/code-creativity-culture/ "code/creativity/culture website") Laptop Ensemble exists to explore different ways to use this instrument in a group performance.
The Laptop Ensemble is an opportunity to get together with like (and unlike!)-minded folks to practice, collaborate and perform live.

More details on the vision and goal of the group can be found in [this document](https://drive.google.com/open?id=1HgzxJ9jvfr4aDVQIVlUhqZn4Eofm78nQujc5q8eKYtQ) on the drive.

<a name="anchor-1"></a><sup>[1](#laptop)</sup> by “laptop” we really mean any *programmable* device where changing the program or configuration of the device is part of the performance

## Team

| Member                    | Uni ID                | Role             |
|:--------------------------|:----------------------|:-----------------|
| Alex Malone               | U6670297              | Music developer  |
| Ben Keough                | U5979137              | Music developer  |
| Brent Schuetze            | U5905548              | Visuals developer|
| Demi Chen                 | U5902022              | Project Manager  |
| Rohan Proctor             | U5581830              | Visuals developer|
| Ruochen Wu                | U6316436              | Music developer  |
| Timothy Lee               | U6018802              | Music developer  |
| Yuze Gong                 | U5640420              | Music developer  |

Client: [Ben Swift](http://benswift.me "Ben's personal blog")  
Substitute Client: [Kieran Browne](https://kieranbrowne.com/ "Kieran's personal blog")  
Communication Channel: [Discord](https://discordapp.com/)

## Gigs
This is our current gig schedule:
* [Completed] [ACT Science Week](https://www.facebook.com/events/200761867283757/), Location: Pop-Up at ANU, 9-9:30pm, Friday 10th Aug
* [Completed] [ANU Open Day](https://www.facebook.com/events/458045014711773/), Location: MSI, Saturday 25th August
* [Completed] [Sounding Canberra Performance](https://www.facebook.com/events/242945916415942/), Location: Ainslie Arts Center, Friday 5th October
* [Planned] Innovation ACT Awards Night (20th October)

<a name="gig-recordings"></a>  
### Gig recordings 
Vides of completed gigs (and any other videos) will be uploaded to [this youtube channel](https://www.youtube.com/channel/UCIU6SqIS02GJlnLOPqlwmpA).  

**Curious Feast**  
* [Alex Malone & Brent Schuetze (with visuals from Rohan Proctor)](https://youtu.be/J2rwZFJCmHY).  
* [Ben Keough & Brent Schuetze](https://youtu.be/S7yrx0Od-8Q).  
  
**ANU Open Day 2018**  
* [Ushini Attanayake & Brent Schuetze](https://youtu.be/E5W6sKft1t0).  
* [Ben Keough & Brent Schuetze](https://youtu.be/-MPostuRU98).  
* [Alex Malone & Rohan Proctor](https://youtu.be/FZVe4Eb3-kU).  

**Sounding Canberra Debut Album Launch**
* [Alex Malone, Timothy Lee, Thomas La & Rohan Proctor](https://www.youtube.com/watch?v=o7CybsFb7ic&t=)
* [Ben Keough, Demi Chen, Rohan Proctor & Brent Schuetze](https://www.youtube.com/watch?v=MmYU7eWQkic)
* [Yuze Gong , Ruochen Wu & Brent Schuetze](https://www.youtube.com/watch?v=dZAry53i588)


<a name="meeting-minutes"></a>  
## Meeting minutes
 * [Meeting minutes](https://drive.google.com/drive/u/0/folders/1ZvZFeZxwE-PmV46EMIhKdh0mRHBKSPm1 "minutes") 
 * [Tutorial meeting Week2 31/07](https://docs.google.com/document/d/1Y_Cnu3DTBkEFpDZksG8zKF8pzSFrDiWgJh0cCSR-E1U/edit "Week2 31/07") 
 * [Rehearsal meeting Week3 06/08](https://docs.google.com/document/d/1yCyAOJU9bGe8MScBZW5UvfgrbTqOiO_kpMp3JHMlBjk/edit "Week3 06/08") 
 * [Tutorial meeting Week3 07/08](https://drive.google.com/open?id=1mMH1zbzD4VzEZrwblwE8IOrWfXoYFKXKqW2I10Aum-k "Week3 07/08")
 * [Rehearsal meeting Week4 13/08](https://docs.google.com/document/d/1IMihnO513sFU0dHrsqRuuFKHHtYtLEoJLaK5WanPQgI/edit "Week4 13/08")
 * [Tutorial meeting Week4 14/08](https://docs.google.com/document/d/10jHaiVgMXgTR69Dno5Xi7--59VSw5ElRtX4LlrzY1AE/edit "Week4 14/08")
 * [Admin meeting Week5 20/08](https://docs.google.com/document/d/1nK3eoStY6Fyix5eOOh55LaiUCyOby_W54hGlsoERgrY/edit "Week5 20/08")
 * [Rehearsal meeting Week5 20/08](https://docs.google.com/document/d/1bk1k7TTkWkRAW5T0KmPbPsJJuLACmyobs_YAhagU8KM/edit "Week5 20/08")
 * [Tutorial meeting Week5 21/08](https://docs.google.com/document/d/18HSS-prwkK8jny90G4VmkL6MlsM7DQ2nxkA6pQXGEKk/edit "Week5 21/08")
 * [Admin meeting Week6 27/08](https://docs.google.com/document/d/1Rb_5_xy6T7HyV6UpWSYmeMs0bvaFovq3prbSE6-0nYc/edit "Week6 27/08")
 * [Rehearsal meeting Week6 27/08](https://docs.google.com/document/d/1Y1GJ8I_z13MFm8mckKAGcQ-55NREes0_-SFq66H6v7U/edit "Week6 27/08")
 * [Tutorial meeting Week6 28/08](https://docs.google.com/document/d/1WMBbXVjEnANTXAWelY3MxSePqFA_6bRHcGic1Dt_29g/edit "Week6 28/08")
 * [Admin meeting Teaching break Week](https://docs.google.com/document/d/1xeFrGrDcbbY8h5Zngo0S782VbfJ4KEjeVPg9unp529w/edit "Teaching Break Week 10/09")
 * [Rehearsal meeting Teaching break Week](https://docs.google.com/document/d/1s-zzrtytstwTzTCwvcg3_SuCAA5COvC37nfuXI9BZMI/edit "Teaching Break Week")
 * [Admin meeting Week7 17/09](https://docs.google.com/document/d/1o-4XsDEZHMIy084rrWGjwRVkGlfUzNhkmLiP0qj5g84/edit "Week7 17/09")
 * [Rehearsal meeting Week7 17/09](https://docs.google.com/document/d/12PsudQKLOUaP2-8TrZxuvQmuQlqB_QRZOyQyFxr4B8o/edit "Week7 17/09")
 * [Tutorial meeting Week7 18/09](https://docs.google.com/document/d/1sA0ZIgT7Yuoz4g6uMApOJ6QbArS-XHr2DziymdG5pW4/edit "Week7 18/09")
 * [Admin meeting Week8 24/09](https://docs.google.com/document/d/1o-4XsDEZHMIy084rrWGjwRVkGlfUzNhkmLiP0qj5g84/edit "Week8 24/09")
 * [Rehearsal meeting Week8 24/09](https://docs.google.com/document/d/1p36tM6g7Zz2L5iN3tgQ1TCKc2M2Nr4IqHFoR2WKxL80/edit "Week8 24/09")
 * [Poster meeting Week8 28/09](https://docs.google.com/document/d/1GUQV-rDpFweWXQqyeL-FrOD7fXMlsoTB-QsVQQ0FVSU/edit "Week8 28/09")
 * [Rehearsal meeting Week9 01/10](https://docs.google.com/document/d/1zEIiiWjSb3yEVQK0a2LP-p8OfgUaAKK1ufOfiIkMvaw/edit "Week9 01/10")
 * [Tutorial meeting Week9 02/10](https://docs.google.com/document/d/1lbi8yf55li1NxFvMjXSNTd35425_YR9cEYR0HYwO4AI/edit "Week9 02/10")  
## Rehearsal
Rehearsals will take place in Peter Karmel's "Listening Space" every Monday from 4pm to 8pm.  
Minutes / notes relating to the rehearsals can be found [here](https://drive.google.com/open?id=19MLrJoi9l0xB73wOr3qr-vf5_jl2ezwF).  
Photos of the rehearsals can be found [here](https://drive.google.com/open?id=12wSAswVKKxtX0KIw1iPY9B7kBKXnwolb) and location can be found [here](https://drive.google.com/open?id=108zoydrxIXh_DG98fxRQ2J4QF0LVJDNS).

## Diaries
Each member will keep a diary to log what they have been doing for each week towards the project.  
These diaries can be found [here](https://drive.google.com/open?id=13lp9J8VFwJHu8zEdHMKw9TEs_0cJXoQE).

## Plan

For internal tracking of tasks and issues we are using trello, our group can be seen [here](https://trello.com/laptopensemble).

* Week 3: [ACT Science Week Gig](#gigs)
* Week 5: [ANU Open Day 2018 Gig](#gigs)
* Mid Sem Break: Develop Framework for group composition/performance 
* Week 9: [Sounding Canberra Performance](#gigs)
* Week 11: Innovation ACT Awards Night Performance

## Links  

### Facebook
[This is the facebook group](https://www.facebook.com/ANULaptopEnsemble/) for the laptop ensemble, we're using it to help advertise / promote any performances and share what we're been working on.

### Youtube
[This is the youtube channel](https://www.youtube.com/channel/UCIU6SqIS02GJlnLOPqlwmpA) for the laptop ensemble, we're using it to post videos of our performances etc.

### Trello
[This is our trello group](https://trello.com/laptopensemble) for the laptop ensemble, we're using it to track tasks and issues for each member and the group as a whole.

### Google Drive
[This is the google drive](https://drive.google.com/open?id=15CYahK-YXfkauBzb-o4cklJhJKCOuNIu) for the laptop ensemble, it contains most of the administrative aspects and some larger file storage.  


## Stakeholders

**Ben Swift**  
Ben is the main client for the Laptop Ensemble, he has helped us with the overall vision of the group and is letting us have creative freedom over what we want to achieve and produce. Ben will also be participating within the group itself, providing equipment if necessary, offering suggestions, ideas and even collaboratively performing with the group.  

**Kieran Browne**  
Kieran is the substitute client for the group while Ben is away on leave. He is a live coder himself and a leading member of the code / creativity / culture group. Kieran works very closely with Ben as his PHD student and has similar expectations when it comes to artifacts / performances and project output for the group.

**Alexander (Alec) Hunter**  
Alec is the tutor and pseudo client for the Laptop Ensemble, he has high expectations of the group and always pushes us to do our best. Alec also helps us with the audio / visual equipment, promotions and potential collaborations and generally participates in the group conversations with suggestions / ideas and feedback. What he wants to see out of us is more organization and structure (composition) in our planning, performances and output.



<a name="team-bios"></a>  
## Team Bios

**Alex Malone**  
I have a background in electronic music production and live performance through Ableton. I also have experience in networking and programming. I've done numerous live gigs before via the use of midi devices and live sound manipulation. I'm interested in seeing the possibilities that come alongside the use of hardware and interconnectivity throughout midi devices and sound manipulation.

**Ben Keough**  
I have a mixed background in physics/elec engineering and as a experimental musician/composer/improviser, most of my prior experience is instrumental/hardware-based electroacoustic music so I'm interested in expanding my fluency in using computers in a performance setting

**Brent Schuetze**  
I have some decent experience in p5 and programming in general, but this will be my first time moving in to live performance / live coding. On the music side of things I used to play drums and bass but that was a long time ago now. I'm interested in projection mapping and creating visuals to complement audio performances.

**Demi Chen**  
I have experience in project management and organization from Techlauncher, on the music side I used to play alto saxophone in orchestra. I am insterested in project management in this project because it combines two of my experiences together, and I am looking for more experience in in manager role.

**Rohan Protor**  
I'm interested in the visual side of the project, having worked with processing quite a bit previously. Haven't done any live coding before and think creating visuals on the fly will be a cool experience and skill to learn. Music wise, I play the trombone and DJ around Canberra a bit. Keen to get cracking with live coded GLSL shaders. 

**Ruochen Wu**  
I am a CSIT student who loves music and has experience in piano, vocals, choir music, etc., but the laptop ensemble is an exciting brand new world for me. From the laptop ensemble, I aim to pursue a deeper understanding in electronic music, synthesis, composing, live coding, and of course, friends and fun. 

**Timothy Lee**  
I'm a computer science student who also had a background in playing flute & piano. During the course comp2300, the assignments involved making music on a microcontroller. I enjoyed doing this so much that when I heard that the lecturer for that course was creating a laptop orchestra project, I dedided to join.

**Yuze Gong**  
I have always been interested in music and want to find a way to express my ideas. I've tried a great variety of musical instruments, including piano, guitar, harmonica. Coming across with the laptop ensemble project is very fortunate for me since I could finally quit practising music and purely focus on the composition on music.


<a name="equipment-tools"></a>  
## Equipment / Tools 
Live Coding Visual Language: p5.js  
[<img src="https://pbs.twimg.com/profile_images/502135348663578624/-oslcYof.png" alt="P5 logo" width="150px"/>](https://p5js.org/)

Visual Programming Language: Puredata  
[<img src="http://minchee.org/wp-content/uploads/2015/10/4556521354_ca5b5f61a5.jpg" alt="Pd logo" width="150px"/>](https://puredata.info/)

Live Coding Audio Language: Sonic-Pi  
[<img src="https://sonic-pi.net/media/images/home/logo.png" alt="Sonic-pi logo" width="150px"/>](https://sonic-pi.net/)

Digital Audio Workstation: Ableton Live  
[<img src="https://www.doc.gold.ac.uk/eavi/rapidmix.goldsmithsdigital.com/wp-content/uploads/2016/03/v02_E789B8ABB09109A8986D289930E232EB.png" alt="Ableton logo" width="150px"/>](https://www.ableton.com/en/)

Digital Audio Production Application: REAPER  
[<img src="https://www.reaper.fm/v5img/logo.jpg" alt="Reaper logo" width="150px"/>](https://www.reaper.fm/)

### Potential Tools
Live Coding Cyberphysical Language: Extempore  
[<img src="https://extemporelang.github.io/img/logonav.png" alt="Extempore logo" width="150px"/>](https://extemporelang.github.io/)

Music Production Environment: FL Studio  
[<img src="https://www.mushroom-magazine.com/site/wp-content/uploads/2013/12/FLStudio_Fruit_Logo.jpg" alt="Fl logo" width="150px"/>](https://www.image-line.com/flstudio/)
 

[Return to top](#laptop-ensemble)