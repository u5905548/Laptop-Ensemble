use_debug false
use_bpm 130

define :fullstop do |n|
  return ([0.25] * n + [0]).ring
end

live_loop :clock do
  sleep 1
end

fadein = (ramp * range(0, 1, 0.01))

kick_cutoff = range(50, 80, 0.5).mirror
live_loop :kick do
  sync :clock
  4.times do
    if (spread 1, 4).tick then
      sample :bd_tek, amp: fadein.look, cutoff: kick_cutoff.look
    end
    sleep (fullstop 3).look
  end
end

define :snare do |amp|
  sample :sn_dolf, amp: amp, start: 0.15, finish: 0.35, rate: 0.7
end

live_loop :snares do
  snare 1 * fadein.tick
  sleep 1.5
  sync :clock
end

live_loop :snare_break do
  sync :snares
  sleep 15.75
  with_fx :reverb, mix: 0.3, room: 0.8 do
    with_fx :echo, mix: 0.4, decay: 12, phase: 0.75 do
      snare 0.5
    end
  end
end

live_loop :hats do
  stop
  sync :clock
  2.times do
    if (spread 3, 8).tick then
      with_fx :rhpf, cutoff: 125, res: 0.8 do
        with_synth :pnoise do
          play :d1, attack: 0.05, decay: 0.08, release: 0.1
          sleep 0.25
        end
      end
    end
  end
end

live_loop :noise_hats do
  ##| stop
  sync :clock
  with_fx :slicer, mix: 1, phase: 0.25, pulse_width: 0.1 do
    with_fx :hpf, cutoff: 130 do
      with_synth :noise do
        play :d1, sustain: 1
        sleep 0
      end
    end
  end
end

bassline_rhythm = (ring 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0.5, 0, 1, 0, 0.5, 0)
bassline_notes = (stretch [:d1]*12 + [:f1, :f1, :a1, :f1], 8)
live_loop :bass_line do
  sync :clock
  4.times do
    with_synth :fm do
      play bassline_notes.tick,
        amp: bassline_rhythm.look * fadein.look,
        attack: 0.03,
        divisor: 1,
        depth: 2.5
      sleep 0.25
    end
  end
end

dchord = chord(:d2, :minor, num_octaves: 3)
live_loop :synth do
  puts '##########################################################'
  sync :clock
  3.times do
    with_fx :reverb, mix: 0.4, room: 0.6, damp: 0.8 do
      with_synth_defaults release: 0.3 do
        with_synth :sine do
          play_chord dchord
        end
        with_synth :dsaw do
          play_chord dchord
        end
      end
    end
    sleep (ring 1.5, 1.5, 0.9).tick
  end
end

live_loop :revbassline do
  sync :snares
  sleep 7.5
  with_fx :pan, pan: -0.5 do
    with_synth :fm do
      play :d1, attack: 0.5, divisor: 0.5, depth: 6
    end
  end
  sleep 0.5
end

live_loop :one_bar do
  puts '*************************************************************************************************************'
  sync :clock
  4.times do
    sleep (ring 1, 1, 1, 0).tick
  end
end
