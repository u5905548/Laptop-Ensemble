live_loop :sound_testing do
  stop
  synths = (ring, synth_names[0])
  melody = scale(:c4, :major)
  
  define :change_attri do |i, s|
    synths = (ring, synth_names[i])
    melody = scale(s, :major)
  end
  
  change_attri 3, :c4
  print synths
  
  synths.size.times do
    use_synth synths.tick
    melody.size.times do
      play melody.tick(:melody)
      sleep 0.5
    end
  end
end

use_bpm 124

lead_p1 = [:c5, :g4, :bf4, :c5, :g4, :bf4,  :c5]
lead_p2 = [:f5, :ds5, :g5, :f5, :ds5, :ds5, :c5]
lead_r1 = [1, 1, 1, 2, 1, 1, 1]

lead_melody = lead_p1 + lead_p2*2 + lead_p1.take(4)
lead_rhythm = lead_r1*3 + lead_r1.take(3) + [5]

live_loop :lead do
  with_synth :chiplead do
    play lead_melody.ring.tick
    sleep (lead_rhythm.ring.look)/2.0
  end
end
