// audio in reference: https://p5js.org/reference/#/p5.AudioIn
// sound reference:    https://p5js.org/reference/#/libraries/p5.sound
// fft reference:      https://p5js.org/reference/#/p5.FFT


// new p5.FFT([smoothing 0 - 1], [bins])
// setInput() - set the input of FFT
// waveform() - returns amplitudes between -1.0 and 1.0
// analyze() - return array of amplitude values (0 - 255)
//             can specify power 2 bins (min 16)

// smooth() - smooth fft analysis by averaging

// getEnergy() args: "bass", "lowMid", "mid", "highMid", "treble"
//                   range 0 - 255 (call analyze() first)



var audio;
var audiofft;
var audiofft_alt;
var spectLength;
var waveLength;

var blocks;

var test;
var cluster;

function setup() {
    createCanvas(windowWidth, windowHeight);

    audio = new p5.AudioIn();
    audio.start();
    audio.amp(0.3);
    
    audio_alt = new p5.AudioIn();
    audio_alt.start();
    audio_alt.amp(0.3);

    audiofft = new p5.FFT(0.7);
    audiofft.setInput(audio);

    audiofft_alt = new p5.FFT(0.1);
    audiofft_alt.setInput(audio_alt);

    audiofft_alt_b = new p5.FFT(0.1);
    audiofft_alt_b.setInput(audio_alt);

    console.log(audio.getSources());

    spectLength = Math.pow(2, 5);
    waveLength = Math.pow(2, 7);

    test = new Ball(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height), 10, random(2, 4), [128]);
    cluster = new Cluster(7, [255, 100, 100], [0, 0, 100], 2, true);
    cluster.spawn();
}

function draw() {
    // audioLevel = audio.getLevel();
    spectrum = audiofft.analyze(spectLength);
    spectrum_alt = audiofft_alt.analyze();
    spectrum_alt_b = audiofft_alt_b.analyze();
    // let spectrumCopy = spectrum.splice(0,spectLength);
    // waveform = audiofft.waveform(waveLength);
    // waveLength = waveform.length;
    let lowMid = audiofft.getEnergy("lowMid");
    let treble = audiofft_alt.getEnergy("treble");
    let bass = audiofft_alt.getEnergy("bass");
    // bass = bass * 1.5;
    background(0);

    // test.move();
    // test.draw(true);

    cluster.update(lowMid);
    cluster.updateLinkWeight(treble);
    cluster.updateLinkColour(bass);
    cluster.moveBalls();
    cluster.draw();
}

function createBall() {
    return new Ball(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height), 10, random(2, 4), [random(255), random(255), random(255)]);
}

function Cluster(size, colour, strcolour, strWeight, colouringBalls) {
    this.size = size;
    this.colour = colour;
    this.strokeColour = strcolour;
    this.strokeWeight = strWeight;
    this.colouringBalls = colouringBalls;
    this.balls = [];
    this.linkMin = 1;
    this.linkMax = 20;

    this.spawn = function() {
        for (let i = 0; i < this.size; i++) {
            this.balls[i] = createBall();
        }
    }

    this.update = function(audioLevel) {
        this.colour[3] = audioLevel;
    }

    this.updateLinkWeight = function(audioLevel) {
        this.strokeWeight = map(audioLevel, 0, 255, this.linkMin, this.linkMax);
    }

    this.updateLinkColour = function(audioLevel) {
        this.strokeColour[0] = ((frameCount/5) + map(audioLevel, 0, 255, 0, 359)) % 360;
        this.strokeColour[1] = map(audioLevel, 0, 255, 0, 100);
    }

    this.moveBalls = function() {
        for (let i = 0; i < this.balls.length; i++) {
            this.balls[i].move();
        }
    }

    this.draw = function() {
        // create shape and draw this.balls
        push();
            fill(this.colour);
            // noStroke();
            // beginShape();
            //     for (let i = 0; i < this.balls.length; i++) {
            //         vertex(this.balls[i].position.x, this.balls[i].position.y);
            //     }
            // endShape();
            //noStroke();
            colorMode(HSB);
            stroke(this.strokeColour);
            strokeWeight(this.strokeWeight);
            // noFill();
            colorMode(RGB);
            fill(this.colour);
            beginShape(TRIANGLE_STRIP);
                for (let i = 0; i < this.balls.length; i++) {
                    vertex(this.balls[i].position.x, this.balls[i].position.y);
                }
            endShape(CLOSE);

            for (let j = 0; j < this.balls.length; j++) {
                this.balls[j].draw(this.colouringBalls);
            }
        pop();
    }
}

function Ball(xin, yin, size, speed, colour) {
    this.position = createVector(xin, yin);
    this.movement = createVector(0, 0);
    this.direction = createVector(0, 0);
    this.destination = createVector(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height));
    this.size = size;
    this.speed = speed;
    this.colour = colour;
    this.timeToDest = 0;

    this.move = function() {
        this.timeToDest = this.timeToDest + 1;
        this.direction.set(this.position.x - this.destination.x, this.position.y - this.destination.y);
        this.direction.normalize();
        this.direction.mult(-1);
        this.movement.add(this.direction.x*0.08, this.direction.y*0.08);
        this.movement.normalize();
        this.movement.mult(this.speed);
        this.position.add(this.movement);
        if (this.position.dist(this.destination) < 100 || this.timeToDest > 400) {
            this.timeToDest = 0;
            this.destination.set(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height));
        }
    }

    this.draw = function(coloured) {
        push();
            if (coloured) {
                stroke(this.colour);
            } else {
                stroke(255);
            }
            strokeWeight(this.size);
            point(this.position.x, this.position.y);
        pop();
    }
}



