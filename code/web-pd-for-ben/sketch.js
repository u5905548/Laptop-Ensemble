var patch;

// To change the pd file, place it in the "assets" folder and then update this string to the new name
// eg. "assets/name.pd"
//     \/\/\/\/\/\/\/\/\/\/
$.get("assets/iOSTestv3.pd", function (patchStr) {
    patch = Pd.loadPatch(patchStr);
    Pd.start();

    // If you need to send an initialization message, put it here
    // \/\/\/\/\/\/\/\/\/\/\/\/
    // Pd.send("Name", [Value]);

})

var sliders;
var numSliders;
var sliderOffset;

function setup() {
    createCanvas(windowWidth, windowHeight);

    // Enter the number of sliders here:
    numSliders = 4;

    sliders = [];
    sliderOffset = 0;

    // Add details of sliders here:
    // comma (,) separates inputs, which are in order
    // (x position, y position, width, height, output range start, output range finish, "object name")
    // You'll mostly want to change the last 3 values, also the sliders currently default to the starting value
    // To add a slider, copy the two lines (.push and ++) and change the last 3 values as necessary (start does
    // not have to be lower than finish)
    sliders.push(new Slider((width / numSliders) * sliderOffset, 0, (width / numSliders), height, 0, 1, "ModPitch"));
    sliderOffset++;
    sliders.push(new Slider((width / numSliders) * sliderOffset, 0, (width / numSliders), height, 0, 1, "Mod"));
    sliderOffset++;
    sliders.push(new Slider((width / numSliders) * sliderOffset, 0, (width / numSliders), height, 0, 1, "MainPitch"));
    sliderOffset++;
    sliders.push(new Slider((width / numSliders) * sliderOffset, 0, (width / numSliders), height, 0, 1, "Output"));
    sliderOffset++;
}

function draw() {
    background(0);

    for (let i = 0; i < sliders.length; i++) {
        sliders[i].update();
        sliders[i].display();
        sliders[i].sendPD();
    }
}

function Slider(x, y, width, height, start, stop, name) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.start = start;
    this.stop = stop;
    this.currentValue = start;
    this.previousValue = start;
    this.receiverName = name;

    this.display = function() {
        fill(125);
        stroke(0);
        strokeWeight(1);
        rect(this.x, this.y, this.width, this.height);

        fill(255, 0, 0);
        rect(this.x, map(this.currentValue, this.start, this.stop, this.y, this.height) - 4,this.width, 9);
    }

    this.update = function() {
        if (mouseIsPressed) {
            if (mouseX >= this.x && mouseX <= this.x + this.width && mouseY >= this.y && mouseY <= this.y + this.height) {
                this.currentValue = map(mouseY, 0, height, this.start, this.stop);
            }
        }
    }

    this.sendPD = function() {
        if (this.currentValue != this.previousValue) {
            this.previousValue = this.currentValue;
            Pd.send(this.receiverName, [this.currentValue]);
        }
    }
}