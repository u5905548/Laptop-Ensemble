// Rohan Proctor 2018

var audio;
var audiofft;
var testSongInput;
var spectrum;

var ball;
var indicator;

var rotationDrag = 0.001;

var boxPoints = [];
var pointArrsContainer = [];

var linkedShapeArr = [];

var easycam;

var moveActivated = false;

var moveIndex = 1;
var lerpP = 0;

var trailLength = 10;

var isRotating = false;

var song;
var spectrum;

var r = 0;

var rotationVal = 0;

var isRotatingY = true;
var isRotatingZ = false;

var showTrails = false;
var drawingConnectors = false;

var hueVal = 0;

var bigR = 0.05;
var backOn = false;
var backOut = false;
var backTimer = 0;

var sceneFrameCount = 0;

function preload() {
    song =  loadSound('song.mp3');
}

function setup() {
    createCanvas(windowWidth, windowHeight, WEBGL);
    colorMode(HSB);

    setAttributes('antialias', true);

    // audio = new p5.AudioIn();
    // // audio.setSource(0);

    // audio.start();
    // audio.amp(0.5); 

    song.loop();
    song.setVolume(0.5);

    audiofft = new p5.FFT(0.05, 32); // Using 64 bin spectrum
    audiofft.setInput(song);

    instantiateLerpShapes();

    for (let i = 0; i < pointArrsContainer[0].length; i++) {
        linkedShapeArr.push(new LinkedShape(pointArrsContainer[0][i][0],pointArrsContainer[0][i][1],pointArrsContainer[0][i][2]));
    }

}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
    easycam.setViewport([0,0,windowWidth, windowHeight]);
}

function draw() {
    background(0);
    translate(0,0,-900);

    spectrum = audiofft.analyze();

    // Lower bass weighting
    for (let i = 0; i < 5; i++) {
        spectrum[i] *= (0.7 + 0.05*i);
    }

    /*
        TODO:

        Needs:
         - Change between different shapes
         - General automation
    */

    // Big backround

    if (backOn) {
        for (let i = 0; i < 10; i++) {
            push();
                specularMaterial(30);
                translate(sin( i * TWO_PI/10) * 1400 , cos( i * TWO_PI/ 10) * 1400 ,- 5500);
                rotateZ(r / 12);
                box(400,50, - 500 + bigR*400);
            pop();
        }

        if (bigR < 35) {
            bigR += 0.05;
        } else {
            backTimer += 1;
        }

        if (backTimer == 60 * 24) {
            backOn = false;
            backOut = true;
        }
    } 
    if (backOut) {
        for (let i = 0; i < 10; i++) {
            push();
                specularMaterial(30);
                translate(sin( i * TWO_PI/10) * 1400 , cos( i * TWO_PI/ 10) * 1400 ,- 5500 + (bigR-35)*1000);
                rotateZ(r / 12);
                box(400,50, - 500 +bigR*400);
            pop();
        }

        if (bigR < 54) {
            bigR += 0.05;
        } else {
            backCycleDone();
        }
    }

    if (isRotating) rotationVal += 0.05;

    if (isRotatingZ) rotateZ(rotationVal/14);
    if (isRotatingY) rotateY(rotationVal/14);
    
    pointLight(hueVal, 150, 160, sin(r) * 200, cos(r)*200, 0);
    pointLight(hueVal, 40, 200, cos(r) * 800, sin(r)*900, 0);
    pointLight(hueVal, 50, 240, 0, sin(r)*900, cos(r) * 800);
    ambientLight(hueVal,200, 20);

    r += 0.05;

    for (let i = 0; i < linkedShapeArr.length; i++) {
        linkedShapeArr[i].update(spectrum[i]);
        linkedShapeArr[i].show();
    }

    if (moveActivated) {
        
        if (lerpP < 1.00) {
            lerpP += 0.01;

            if (lerpP > 1) lerpP = 1;
        } 

        for (let i = 0; i < linkedShapeArr.length; i++) {
            linkedShapeArr[i].moveShape(pointArrsContainer[moveIndex][i][0], pointArrsContainer[moveIndex][i][1],pointArrsContainer[moveIndex][i][2], lerpP);
        }

        if (lerpP == 1) {
            moveActivated = false;
            lerpP = 0;
            moveIndex++;

            for (let i = 0; i < linkedShapeArr.length; i++) {
                linkedShapeArr[i].setOldPos(pointArrsContainer[moveIndex-1][i][0], pointArrsContainer[moveIndex-1][i][1],pointArrsContainer[moveIndex-1][i][2]);
            }
        }
    }

    if (drawingConnectors) {
        for (let i = 0; i < linkedShapeArr.length; i++) {
            for (let j = 0; j < linkedShapeArr.length; j++) {

                if (linkedShapeArr[i].intensity > 190 && linkedShapeArr[j].intensity > 150 ) {

                    if (random() < 0.35)  {
                    drawConnector(linkedShapeArr[i].position.x,
                        linkedShapeArr[i].position.y,
                        linkedShapeArr[i].position.z, 
                        linkedShapeArr[j].position.x,
                        linkedShapeArr[j].position.y,
                        linkedShapeArr[j].position.z);
                    }
                }
            }
        }
    }

    ///////////////////// AUTOMATION /////////////////////////
    
    // rotate y vs z should only swap at 10 sec intervals

    if (sceneFrameCount > 60 * 10) {
        if (sceneFrameCount % (60 *6) == 0) {
            changeShape();          
        }
    }

    if (sceneFrameCount == 60 * 30) {
        isRotating = true;
    }

    if (sceneFrameCount == 60 * 45) {
        showTrails = true;
    }

    if (sceneFrameCount == 60 * 60) {
        showTrails = false;
        drawingConnectors = true;
    }

    if (sceneFrameCount == 60 * 75) {
        changeShape();
        showTrails = true;
    }

    if (sceneFrameCount == 60 * 80) {
        changeShape();
        isRotatingZ = true;
    }

    if (sceneFrameCount == 60 * 90) {
        triggerBackShapes();
    }

    if (sceneFrameCount == 60 * 150) {
        triggerBackShapes();
    }

    if (sceneFrameCount % 60 == 0) {
        console.log(sceneFrameCount / 60);
        
    }

    sceneFrameCount++;    
}

var ranHolder = 0;

function backCycleDone() {
    backOn = false;
    backOut = false;
    backTimer = 0;
    bigR = 0;
}

function triggerBackShapes() {
    backOn = true;
}

function drawConnector(x1,y1,z1,x2,y2,z2) {
    var shapeW = 10;

    push();
        specularMaterial(200);
        beginShape();
            vertex(x1 - shapeW, y1 - shapeW, z1);
            vertex(x2 - shapeW, y2, z2);
            vertex(x2 + shapeW, y2, z2);
            vertex(x1 + shapeW, y1 + shapeW, z1);
        endShape();   
    pop();
}

function changeShape() {
    moveActivated = true;
    var ran;
    for (let i = 0; i < 4; i++) {
        ran = floor(random(0, pointArrsContainer.length - 1));
        if (moveIndex != ran) {
            break;
        }
    }
    moveIndex = ran;
    hueVal = random(0,360);
}

function keyPressed() {
    switch (keyCode) {
        case 32: // SPACE
            moveActivated = true;
            var temp = moveIndex;
            var ran = moveIndex;
            while (temp == ran) {
                ran = floor(random(0, pointArrsContainer.length - 1));
            }
            moveIndex = ran;
            hueVal = random(0,360);
            break;
        case 90: // Z

            isRotating = !isRotating;
            
            break;
        case 88: // X

            if (!isRotatingY || !isRotatingZ ) {
                isRotatingZ = true;
                isRotatingY = true;
            } else if (isRotatingY && isRotatingZ) {
                isRotatingZ = false;
            }
            break;
        case 67: // C
            triggerBackShapes();
            break;
    }
}

function LinkedShape (x,y,z) {
    this.position = createVector(x,y,z);
    this.oldPos = createVector(x,y,z);
    this.rotation = createVector(0,0,0);

    this.trailPositions = [];

    this.fills = [hueVal,0,0];
    this.strokeFill = [hueVal,50,100];

    this.intensity = 0;

    this.show = function() {

        push();
        specularMaterial(250);

            if (this.fills[2] == 0) {
                noFill();
            } else {
                fill(this.fills);
            }
            strokeWeight(1);
            stroke(hueVal, 50,100);
            translate(this.position);
            rotateX(this.rotation.x);
            rotateY(this.rotation.y);
            rotateZ(this.rotation.z);
            // sphere(50, 4, 4);
            box();
        pop();

        if (showTrails) {
            this.showTrail();
        }
    }

    this.update = function(intensity) {
        this.intensity = intensity;

        this.fills = [hueVal,50, this.intensity];

        if(frameCount % 3 == 0) {
            for (let i = 0; i < this.trailPositions.length; i++) {
                this.trailPositions[i].shrink();
                if (this.trailPositions[i].radius == 0) this.trailPositions.splice(i,1);
                
            }
        }
    }

    this.showTrail = function() {
        for (let i = 0; i < this.trailPositions.length; i++) {
            this.trailPositions[i].show();
        }
    }

    this.addToTrail = function(x,y,z) {
        this.trailPositions.push(new TrailShape(x,y,z));

        for (let i = 0; i < this.trailPositions.length; i++) {
            this.trailPositions[i].shrink();
            if (this.trailPositions[i].radius == 0) this.trailPositions.splice(i,1);
            
        }
    }

    this.moveShape = function(newPosX, newPosY, newPosZ, l) { 
        this.position.x = lerp(this.oldPos.x, newPosX, l);
        this.position.y = lerp(this.oldPos.y, newPosY, l);
        this.position.z = lerp(this.oldPos.z, newPosZ, l);

        if (floor(l*100) % 3 == 0 ) {
            this.addToTrail(this.position.x, this.position.y, this.position.z);
        }
    }

    this.setOldPos = function(x,y,z) {
        this.oldPos.x = x;
        this.oldPos.y = y;
        this.oldPos.z = z;
    }


}

function BackGroundPlane(x,y,z, w, h) {
    this.pos = createVector(x,y,z);
    this.width = w;
    this.height = h;

    this.show = function() {

        push();
            translate(this.pos);
            plane(this.width, this.height);
        pop();
    }
}



function TrailShape(x,y,z) {
    this.x = x;
    this.y = y;
    this.z = z;

    this.radius = 40;

    this.show = function() {
        push();
            noStroke();
            var v = createVector(this.x,this.y,this.z);
            translate(v);
            specularMaterial(200);
            box(this.radius,this.radius,this.radius);
        pop();
    }

    this.shrink = function() {
        this.radius -= 1;
    }
}

// Funcitons for the 3D shape presets + some random assortments
// All arrangements have tp have 27 LinkedShapes in them

function instantiateLerpShapes() {

    // Big circle
  
    for (let i = 0; i < 27; i++) {
        x = cos(i * TWO_PI/27) * 600;
        y = sin(i * TWO_PI/27) * 600;

        boxPoints.push([x,y,0]);
    }

    pointArrsContainer.push(boxPoints);
    boxPoints = [];

    // Road spike

    for (let i = 0; i < 6; i++) {
        
        boxPoints.push([ i * 100, i* 100, i* 100]);
        boxPoints.push([ -i * 100, -i* 100, i* 100]);

        boxPoints.push([ (i-6) * 100 + 600, -i* 100, -(i-6)* 100 - 600]);
        boxPoints.push([ -(i-6) * 100 - 600, i* 100, -(i-6)* 100 - 600]);
    }

    boxPoints.push([0,0,0],[0,0,0],[0,0,0]);

    pointArrsContainer.push(boxPoints);
    boxPoints = [];

    // Cube

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            for (let k = 0; k < 3; k++) {
                boxPoints.push([ (i - 1) * 80, (j-1) * 80, (k-1) * 80]);
            }   
        }
    }

    pointArrsContainer.push(boxPoints);
    boxPoints = [];

    // Big Cube

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            for (let k = 0; k < 3; k++) {
                boxPoints.push([ (i - 1) * 300, (j-1) * 300, (k-1) * 300]);
            }   
        }
    }

    pointArrsContainer.push(boxPoints);
    boxPoints = [];


    // Close circle
  
    for (let i = 0; i < 27; i++) {
        x = cos(i * TWO_PI/27) * 150;
        y = sin(i * TWO_PI/27) * 150;

        boxPoints.push([x,y,0]);
    }

    pointArrsContainer.push(boxPoints);
    boxPoints = [];

    // Sin Curve 

    for (let i = 0; i < 9; i++) {
        for (let k = 0; k < 3; k++) {

            y = sin(i * TWO_PI / 9) * 90;
            boxPoints.push( [(i - 4)*70, y, (k-1)*90]);

        }   
    }

    pointArrsContainer.push(boxPoints);
    boxPoints = [];

    // Big Cos Curve 

    for (let i = 0; i < 9; i++) {
        for (let k = 0; k < 3; k++) {

            y = cos(i * TWO_PI / 9) * 300;
            boxPoints.push( [(i - 4)*260, y, (k-1)*300]);

        }   
    }

    pointArrsContainer.push(boxPoints);
    boxPoints = [];

    for (let a = 0; a < 5; a++) {
        
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                for (let k = 0; k < 3; k++) {

                    boxPoints.push([random(-10,10)*50,random(-10,10)*50,random(-10,10)*50]);

                }   
            }
        }
    
        pointArrsContainer.push(boxPoints);
        boxPoints = [];
    }
}
