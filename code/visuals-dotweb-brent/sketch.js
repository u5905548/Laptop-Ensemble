// audio in reference: https://p5js.org/reference/#/p5.AudioIn
// sound reference:    https://p5js.org/reference/#/libraries/p5.sound
// fft reference:      https://p5js.org/reference/#/p5.FFT


// new p5.FFT([smoothing 0 - 1], [bins])
// setInput() - set the input of FFT
// waveform() - returns amplitudes between -1.0 and 1.0
// analyze() - return array of amplitude values (0 - 255)
//             can specify power 2 bins (min 16)

// smooth() - smooth fft analysis by averaging

// getEnergy() args: "bass", "lowMid", "mid", "highMid", "treble"
//                   range 0 - 255 (call analyze() first)



var audio;
var audiofft;
var spectLength;
var waveLength;
var dots;
var circle;

function setup() {
    createCanvas(windowWidth, windowHeight);
    // any additional setup code goes here
    audio = new p5.AudioIn();
    audio.start();
    audio.amp(0.3)
    audiofft = new p5.FFT(0.7);
    audiofft.setInput(audio);
    console.log(audio.getSources());

    spectLength = Math.pow(2, 6);
    waveLength = Math.pow(2, 7);

    // Dot web
    dots = new DotWeb(1);
    // dots.generateDots(100, 0);
    dots.setRegionPopulation(100);

    // Circle
    circle = new ReactiveCircle(width/2, height/2, 100, 200, 255);
}

function draw() {
    background(0);
    // audioLevel = audio.getLevel();
    spectrum = audiofft.analyze(spectLength);
    // waveform = audiofft.waveform(waveLength);
    // waveLength = waveform.length;
    //console.log(spectrum[0])

    bass = audiofft.getEnergy("lowMid");


    // Dotweb
    dots.updateRegionEnergy(bass, 0);
    // dots.updateRegionEnergy(bass, -1);
    dots.move();
    dots.decayDots();
    dots.drawDots();
    dots.drawLinks();
    if (frameCount % 10 == 0) {
        dots.maintainPopulation();
    }


    // circle
    circle.updateEnergy(bass);
    circle.move();
    circle.colorize();
    circle.draw();

    // fill(255);
    // noStroke();
    // ellipse(width / 2, height / 2, 100 + map(bass, 0, 255, 0, 200), 100 + + map(bass, 0, 255, 0, 200));



    // noFill();
    // beginShape();
    //     stroke(255, 0, 0); // waveform is red
    //     strokeWeight(1);
    //     for (var i = 0; i < waveform.length; i++) {
    //         var x = map(i, 0, waveform.length, 0, width);
    //         var y = map(waveform[i], -1, 1, 0, height);
    //         vertex(x, y);
    //     }
    // endShape();
}

function ReactiveCircle(xin, yin, siz, gro, col) {
    this.x = xin;
    this.y = yin;
    this.size = siz;
    this.growth = gro;
    this.colour = col;
    this.bright = 100;
    this.sat = 0;
    this.grow = false;
    this.colouring = false;
    this.energy = 0;
    this.eMax = 255;
    this.eMin = 0;
    this.radX = 0;
    this.radY = 0;

    this.draw = function() {
        push();
            colorMode(HSB);
            fill(this.colour, this.sat, this.bright);
            noStroke();
            ellipse(this.x, this.y, this.size + map(this.energy, this.eMin, this.eMax, 0, this.growth));
        pop();
    }

    this.colorize = function() {
        this.colouring = true;
        this.sat = Math.min(this.sat + 0.05, 70);
        this.colour = (this.colour + 1)%361
        if (this.grow) {
            this.bright = this.bright + 0.03;
            if (this.bright >= 70) {
                this.grow = false;
            }
        } else {
            this.bright = this.bright - 0.03;
            if (this.bright <= 20) {
                this.grow = true;
            }
        }
    }

    this.move = function() {
        this.radX = this.radX + random(0.005, 0.01);
        this.radY = this.radY + random(0.001, 0.01);
        this.x = this.x + cos(this.radX)*4;
        this.y = this.y + cos(this.radY)*2;
    }

    this.updateEnergy = function(e) {
        this.energy = e;
    }
}

function Dot(xin, yin, siz, col) {
    this.x = xin;
    this.y = yin;
    this.size = siz;
    this.colour = col;
    this.fade = 1;
    this.colour.setAlpha(this.fade);
    this.growing = true;
    this.population = -1;
    this.vect = createVector(xin - circle.x, yin - circle.y);
    this.vect.normalize();
    this.speed = 3;

    this.draw = function() {
        push();
            stroke(this.colour);
            strokeWeight(this.size);
            point(this.x, this.y);
        pop();
    }

    this.age = function() {
        if (this.growing) {
            this.fade = min(this.fade + random(3, 6), 255);
            this.colour.setAlpha(this.fade);
            if (this.fade == 255) {
                this.growing = false;
            }
        } else {
            if (frameCount % 1 == 0) {
                this.fade = max(this.fade - random(1, 3), 0);
                this.colour.setAlpha(this.fade);
            }
        }
    }

    this.expire = function() {
        let boundTol = 20;
        return (this.fade == 0 || this.x < 0 - boundTol || this.x > width + boundTol || this.y < 0 - boundTol || this.y > height + boundTol);
        // return (false);
    }

    this.calculateAngle = function() {
        this.xAngle = (this.x - (width/2));
        this.yAngle = (this.y - (height/2));
    }

    this.move = function() {
        // this.vect = createVector(xin - circle.x, yin - circle.y);
        // this.vect.normalize();
        this.x = this.x + (this.vect.x * this.speed);
        this.y = this.y + (this.vect.y * this.speed);
    }
}

// function createDot(x) {
//     return (new Dot(x, random(height), random(5, 10), color(255)));
// }


function createDot(x) {
    if (random(10) < 1) {
        return (new Dot(circle.x + random(-100, 100), circle.y + random(-100, 100), random(5, 10), color(random(255), random(255), random(255))));
    } else {
        return (new Dot(x, random(height), random(5, 10), color(random(255), random(255), random(255))));
    }
}

function DotWeb(regionsIn) {
    this.regions = regionsIn;
    this.web = new Array(regionsIn);
    this.regionEnergy = [];
    this.population = -1;
    this.populationGrowth = 20;
    this.linkWeight = 1.5;
    this.energyMultiplier = 1;

    for (let regn = 0; regn < this.web.length; regn++) {
        this.web[regn] = []
    }

    for (let i = 0; i < this.regions; i++) {
        this.regionEnergy[i] = 0;
    }

    this.updateRegionEnergy = function(energy, region) {
        if (region == -1) {
            for (let regEn = 0; regEn < this.regionEnergy.length; regEn++) {
                this.regionEnergy[regEn] = energy;
            }
        } else {
            this.regionEnergy[region] = energy;
        }
    }

    this.setRegionPopulation = function(pop) {
        this.population = pop;
    }

    this.drawLinks = function () {
        // iterate through regions and do distance detection
        push();
            strokeWeight(this.linkWeight);
            for (let regn = 0; regn < this.web.length; regn++) {
                for (let dotOriginIx = 0; dotOriginIx < this.web[regn].length; dotOriginIx++) {
                    for (let dotDestIx = 0; dotDestIx < this.web[regn].length; dotDestIx++) {
                        if (dotOriginIx != dotDestIx) {
                            if (dist(this.web[regn][dotOriginIx].x, this.web[regn][dotOriginIx].y, this.web[regn][dotDestIx].x, this.web[regn][dotDestIx].y) < (this.regionEnergy[regn] * this.energyMultiplier)) {
                                stroke(this.web[regn][dotOriginIx].colour);
                                line(this.web[regn][dotOriginIx].x, this.web[regn][dotOriginIx].y, this.web[regn][dotDestIx].x, this.web[regn][dotDestIx].y)
                            }
                        }
                    }
                }
            }
        pop();
    }

    this.maintainPopulation = function() {
        if (this.population >= 0) {
            for (let regn = 0; regn < this.web.length; regn++) {
                if (this.web[regn].length < this.population) {
                    // population has fallen, add more
                    for (let dotGenIx = 0; dotGenIx < this.populationGrowth; dotGenIx++) {
                        this.web[regn].push(createDot(random(0, width / this.regions) * (regn + 1)));
                    }
                }
            }
        }
    }

    this.drawDots = function() {
        for (let regn = 0; regn < this.web.length; regn++) {
            for (let dotIx = 0; dotIx < this.web[regn].length; dotIx++) {
                // iterate through dots in region and draw and decay them
                this.web[regn][dotIx].age();
                this.web[regn][dotIx].draw();
            }
        }
    }

    this.move = function() {
        for (let regn = 0; regn < this.web.length; regn++) {
            for (let dotIx = 0; dotIx < this.web[regn].length; dotIx++) {
                // iterate through dots in region and draw and decay them
                this.web[regn][dotIx].move();
            }
        }
    }

    this.decayDots = function() {
        for (let regn = 0; regn < this.web.length; regn++) {
            for (let dotIx = 0; dotIx < this.web[regn].length; dotIx++) {
                // iterate through dots in region, delete decayed dots, spawn new ones?
                if (this.web[regn][dotIx].expire()) {
                    this.web[regn].splice(dotIx, 1);
                }
            }
        }
    }

    this.generateDots = function(amount, region) {
        // IN: amount -> amount of dots to generate
        // IN: region -> region to generate dots in, -1 = all regions

        if (region == -1) {
            for (let i = 0; i < this.regions; i++) {
                if (this.web[i].length == 0) {
                    this.web[i] = [];
                }
                for (let j = 0; j < amount; j++) {
                    this.web[i].push(createDot(random(0, width / this.regions) * (i + 1)));
                }
            }
        } else {
            if (this.web[region].length == 0) {
                this.web[region] = [];
            }
            for (let i = 0; i < amount; i++) {
                //                                                  +1 as region index starts at 0
                this.web[region].push(createDot(random(0, width/this.regions)*(region + 1)));      
            }
        }
    }
}