use_bpm 168
use_random_seed 21
bass_synth = :chipbass
melo_synth = :chiplead

define :arpe do |base, type, dur|
  ch = chord(base, type)
  ch = ch + ring(ch[0] + 12)
  (dur * 2).times do
    tick(:arpe)
    cond = (look(:arpe) % 6 == 0)
    if cond
      a = 1.5
    else
      a = 1
    end
    play ch.pick, amp: a, sustain: 0.5 if (not one_in(6) or cond)
    sleep 0.5
  end
end

define :transfer do |ch|
  if one_in(4)
    ch[0] += 12
  end
  if one_in(4)
    ch[1] -= 12
  end
  return ch
end

define :intro do
  init_b = 0.5
  accel_b = line(init_b, 0.25, steps: 15) + ring(0.25)
  rit_b = line(0.25, init_b, steps: 7) + ring(init_b)
  
  use_synth melo_synth
  4.times do
    ch = (ring :b3, :e4, :fs4, :a4).shuffle
    4.times do
      s = ch.tick(:st)
      play [s, s - 12], sustain: accel_b.tick(:bt)
      sleep accel_b.look(:bt)
    end
  end
  
  ch = (ring :b4, :e5, :fs5, :a5, :fs5, :e5, :b4, :fs4)
  8.times do
    s = ch.tick(:st)
    play [s, s - 12], sustain: rit_b.tick(:bt)
    sleep rit_b.look(:bt)
  end
  
  in_thread do
    use_synth melo_synth
    chs = [[:cs4, :e4], [:ds4, :fs4], [:e4, :gs4],
           [:ds4, :fs4], [:ds4, :c5], [:bs4, :ds5],
           [:e4, :cs5], [:ds4, :b4], [:e4, :cs5]
           ]
    b = [1.5, 1.4, 1.3, 1.2, 1.1, 1, 2, 1, 3]
    sleep 3
    9.times do
      play chs.tick(:st), sustain: b.tick(:bt)
      sleep b.look(:bt)
    end
  end
  
  in_thread do
    use_synth bass_synth
    play [:fs2, :cs3, :fs3], sustain: 7.2
    sleep 7.2
    play [:gs2, :ds3, :gs3], sustain: 3.3
    sleep 3.3
    play [:a2, :e3, :a3], sustain: 2
    sleep 2
    play [:b2, :fs3, :b3], sustain: 1
    sleep 1
    play [:cs3, :gs3, :cs4], sustain: 3
    sleep 3
    cue :p2_end
  end
  
  sync :p2_end
  use_synth bass_synth
  ch = [:cs2, :m]
  3.times do
    arpe(ch[0], ch[1], 3)
  end
end

define :a_melo do
  
  in_thread(name: :drum) do
    16.times do
      sample :drum_bass_hard
      sleep 1
      sample :drum_snare_soft
      sleep 0.5
      sample :drum_snare_soft
      sleep 0.5
      sample :drum_snare_soft
      sleep 1
    end
  end
  
  in_thread do
    use_synth melo_synth
    ch = [[:a3, :d4], [:a3, :e4], [:a3, :f4],
          [:f4, :a4], [:d4, :f4], [:c4, :e4],
          [:bb3, :d4], [:bb3, :d4], [:a3, :c4],
          [:f3, :a3], [:f3, :a3], [:f3, :a3],
          [:a3, :d4], [:a3, :e4], [:a3, :f4],
          [:cs4, :g4], [:cs4, :f4], [:cs4, :e4],
          [:f3, :d4], [:f3, :d4], [:e3, :c4],
          [:f3, :d4], [:f3, :d4], [:f3, :d4]
          ] * 2
    ch = ch.take(ch.size - 6)
    ch.size.times do
      tick
      temp_ch = transfer(ch.look.shuffle)
      play_pattern_timed temp_ch, 0.5, sustain: 0.5
    end
    play [:f3, :d4], sustain: 2
    sleep 2
    play [:e3, :c4], sustain: 1
    sleep 1
    play [:f3, :d4], sustain: 3
    sleep 3
  end
  
  in_thread do
    use_synth bass_synth
    ch = [[:d2, :m, 3], [:c2, :M, 3],
          [:bb1, :M, 3], [:a1, :m, 3],
          [:g1, :m, 3], [:a1, :M, 3],
          [:bb1, :M, 2], [:c2, :M, 1], [:d2, :m, 3]
          ] * 2
    ch = ch.take(ch.size - 3)
    ch.size.times do
      tick
      arpe(ch.look[0], ch.look[1], ch.look[2])
    end
    play [:f2, :bb2], sustain: 2, amp: 1.5
    sleep 2
    play [:g2, :bb2], sustain: 1
    sleep 1
    play [:d2, :a2, :d3], sustain: 3
    3.times do
      play [:a2, :d3], sustain: 0.5
      sleep 1
    end
  end
end

intro
a_melo


