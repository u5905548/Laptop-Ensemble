use_bpm 96
use_synth :pluck

define :play_note do |note|
  key = note[0]
  dur = note[1]
  play key, release: dur
  sleep note[1]
end

live_loop :clock do
  sleep 4
end

define :my_play do |notes|
  notes = notes.ring
  (notes.size).times do
    play_note(notes.tick)
  end
end

define :play_loop do |notes, t, name, synt|
  live_loop name do
    sync :clock
    with_synth synt do
      if t == 0
        loop do
          my_play(notes)
        end
      else
        t.times do
          my_play(notes)
        end
        stop
      end
    end
  end
end

accom_synth = :pluck
bass_synth = :chipbass

intro_part_1 = [
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:ds5, 0.75], [:cs5, 0.75], [:b4, 0.49]
]

intro_part_b = [
  [[:cs3, :gs3], 2], [[:cs3, :fs3], 2],
  [[:cs3, :a3], 2], [[:b2, :ds3], 2]
]

accom_part_1 = [
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:e5, 0.75], [:ds5, 0.75], [:b4, 0.49],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:cs5, 0.75], [:e5, 0.75], [:cs5, 0.5],
  [:ds5, 0.75], [:cs5, 0.75], [:b4, 0.49]
]

accom_part_2 = [
  [:gs4, 0.75], [:gs4, 0.75], [:gs4, 0.5],
  [:fs4, 0.75], [:fs4, 0.75], [:fs4, 0.5],
  [:a4, 0.75], [:a4, 0.75], [:a4, 0.5],
  [[:b4, :fs4], 0.75], [[:b4, :fs4], 0.75], [:fs4, 0.49]
]

accom_part_b = [
  [[:cs3, :gs3], 0.75], [[:gs3, :e4], 0.75], [[:gs2, :cs3], 0.5],
  [[:cs3, :fs3], 0.75], [[:fs3, :e4], 0.75], [[:gs2, :cs3], 0.5],
  [[:a2, :cs3], 0.75], [[:a3, :e4], 0.75], [[:a2, :cs3], 0.5],
  [[:fs3, :b3, :e4], 0.75], [[:fs3, :b3, :ds4], 0.75], [[:fs3, :b3], 0.5],
  [[:cs3, :gs3], 0.75], [[:gs3, :e4], 0.75], [[:gs2, :cs3], 0.5],
  [[:cs3, :fs3], 0.75], [[:fs3, :e4], 0.75], [[:gs2, :cs3], 0.5],
  [[:a2, :cs3], 0.75], [[:a3, :e4], 0.75], [[:a2, :cs3], 0.5],
  [[:fs3, :b3, :ds4], 0.75], [[:fs3, :b3, :cs4], 0.75], [[:fs3, :b3], 0.5]
]

##| play_loop(intro_part_1, 2, :intro_part_1, accom_synth)
##| play_loop(intro_part_b, 2, :intro_part_b, bass_synth)
play_loop(accom_part_2, 0, :accom_part_2, accom_synth)
play_loop(accom_part_1, 0, :accom_part_1, accom_synth)
play_loop(accom_part_b, 0, :accom_part_b, bass_synth)
