// audio in reference: https://p5js.org/reference/#/p5.AudioIn
// sound reference:    https://p5js.org/reference/#/libraries/p5.sound
// fft reference:      https://p5js.org/reference/#/p5.FFT


// new p5.FFT([smoothing 0 - 1], [bins])
// setInput() - set the input of FFT
// waveform() - returns amplitudes between -1.0 and 1.0
// analyze() - return array of amplitude values (0 - 255)
//             can specify power 2 bins (min 16)

// smooth() - smooth fft analysis by averaging

// getEnergy() args: "bass", "lowMid", "mid", "highMid", "treble"
//                   range 0 - 255 (call analyze() first)



var audio;
var audiofft;
var spectLength;
var waveLength;

var blocks;

var test;
var cluster;

var sliders;

function setup() {
    createCanvas(windowWidth, windowHeight);

    // audio = new p5.AudioIn();
    // audio
    // audio.start();
    // audio.amp(0.3);

    // audiofft = new p5.FFT(0.7);
    // audiofft.setInput(audio);

    // console.log(audio.getSources());

    spectLength = Math.pow(2, 5);
    waveLength = Math.pow(2, 7);

    test = new Ball(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height), 10, random(2, 4), [128]);
    cluster = new Cluster(7, [255, 100, 100], [255, 255, 255], 2, true);
    cluster.spawn();

    sliders = [];
    sliders.push(new Slider((width/4) * 0, 0, (width/4), height, 0, 1, "ModPitch"));
    sliders.push(new Slider((width/4) * 1, 0, (width/4), height, 0, 1, "Mod"));
    sliders.push(new Slider((width/4) * 2, 0, (width/4), height, 0, 1, "MainPitch"));
    sliders.push(new Slider((width/4) * 3, 0, (width/4), height, 0, 1, "Output"));
    // sliders.push(new Slider((width/2) * 0, 0, (width/2), height, 0, 1, "StartNoise"));
    // sliders.push(new Slider((width/2) * 1, 0, (width/2), height, 0, 1, "Tempo"));
}

function draw() {
    // audioLevel = audio.getLevel();
    // spectrum = audiofft.analyze(spectLength);
    // let spectrumCopy = spectrum.splice(0,spectLength);
    // waveform = audiofft.waveform(waveLength);
    // waveLength = waveform.length;
    // let lowMid = audiofft.getEnergy("lowMid");
    background(0);

    // test.move();
    // test.draw(true);

    cluster.update(0);
    // cluster.update(lowMid);
    cluster.moveBalls();
    cluster.draw();

    for (let i = 0; i < sliders.length; i++) {
        sliders[i].update();
        sliders[i].display();
        sliders[i].sendPD();
    }
}

function Slider(x, y, width, height, start, stop, name) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.start = start;
    this.stop = stop;
    this.currentValue = start;
    this.previousValue = start;
    this.receiverName = name;

    this.display = function() {
        fill(125);
        stroke(0);
        strokeWeight(1);
        rect(this.x, this.y, this.width, this.height);

        fill(255, 0, 0);
        rect(this.x, map(this.currentValue, this.start, this.stop, this.y, this.height) - 5,this.width, 10);
    }

    this.update = function() {
        if (mouseIsPressed) {
            if (mouseX >= this.x && mouseX <= this.x + this.width && mouseY >= this.y && mouseY <= this.y + this.height) {
                this.currentValue = map(mouseY, 0, height, this.start, this.stop);
            }
        }
    }

    this.sendPD = function() {
        if (this.currentValue != this.previousValue) {
            this.previousValue = this.currentValue;
            Pd.send(this.receiverName, [this.currentValue]);
        }
    }
}

function createBall() {
    return new Ball(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height), 10, random(2, 4), [random(255), random(255), random(255)]);
}

function Cluster(size, colour, strcolour, strWeight, colouringBalls) {
    this.size = size;
    this.colour = colour;
    this.strokeColour = strcolour;
    this.strokeWeight = strWeight;
    this.colouringBalls = colouringBalls;
    this.balls = [];

    this.spawn = function() {
        for (let i = 0; i < this.size; i++) {
            this.balls[i] = createBall();
        }
    }

    this.update = function(audioLevel) {
        this.colour[3] = audioLevel;
    }

    this.moveBalls = function() {
        for (let i = 0; i < this.balls.length; i++) {
            this.balls[i].move();
        }
    }

    this.draw = function() {
        // create shape and draw this.balls
        push();
            fill(this.colour);
            // noStroke();
            // beginShape();
            //     for (let i = 0; i < this.balls.length; i++) {
            //         vertex(this.balls[i].position.x, this.balls[i].position.y);
            //     }
            // endShape();
            //noStroke();
            stroke(this.strokeColour);
            strokeWeight(this.strokeWeight);
            // noFill();
            beginShape(TRIANGLE_STRIP);
                for (let i = 0; i < this.balls.length; i++) {
                    vertex(this.balls[i].position.x, this.balls[i].position.y);
                }
            endShape(CLOSE);

            for (let j = 0; j < this.balls.length; j++) {
                this.balls[j].draw(this.colouringBalls);
            }
        pop();
    }
}

function Ball(xin, yin, size, speed, colour) {
    this.position = createVector(xin, yin);
    this.movement = createVector(0, 0);
    this.direction = createVector(0, 0);
    this.destination = createVector(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height));
    this.size = size;
    this.speed = speed;
    this.colour = colour;
    this.timeToDest = 0;

    this.move = function() {
        this.timeToDest = this.timeToDest + 1;
        this.direction.set(this.position.x - this.destination.x, this.position.y - this.destination.y);
        this.direction.normalize();
        this.direction.mult(-1);
        this.movement.add(this.direction.x*0.08, this.direction.y*0.08);
        this.movement.normalize();
        this.movement.mult(this.speed);
        this.position.add(this.movement);
        if (this.position.dist(this.destination) < 100 || this.timeToDest > 400) {
            this.timeToDest = 0;
            this.destination.set(random(0.1 * width, 0.9 * width), random(0.1 * height, 0.9 * height));
        }
    }

    this.draw = function(coloured) {
        push();
            if (coloured) {
                stroke(this.colour);
            } else {
                stroke(255);
            }
            strokeWeight(this.size);
            point(this.position.x, this.position.y);
        pop();
    }
}

function mouseClicked() {
    // Pd.send("StartNoise", [1]);
}



