// audio in reference: https://p5js.org/reference/#/p5.AudioIn
// sound reference:    https://p5js.org/reference/#/libraries/p5.sound
// fft reference:      https://p5js.org/reference/#/p5.FFT


// new p5.FFT([smoothing 0 - 1], [bins])
// setInput() - set the input of FFT
// waveform() - returns amplitudes between -1.0 and 1.0
// analyze() - return array of amplitude values (0 - 255)
//             can specify power 2 bins (min 16)

// smooth() - smooth fft analysis by averaging

// getEnergy() args: "bass", "lowMid", "mid", "highMid", "treble"
//                   range 0 - 255 (call analyze() first)



var audio;
var audiofft;
var spectLength;
var waveLength;

var blocks;

function setup() {
    createCanvas(windowWidth, windowHeight);

    frameRate(200);

    audio = new p5.AudioIn();
    audio
    audio.start();
    audio.amp(0.3);

    audiofft = new p5.FFT(0.1);
    audiofft.setInput(audio);

    console.log(audio.getSources());

    spectLength = Math.pow(2, 5);
    waveLength = Math.pow(2, 7);

    blocks = new MusicWriter(width * 0.8, 0, "right", 15, 1, 30, 20, false, 170, 5);
}

function draw() {
    // audioLevel = audio.getLevel();
    spectrum = audiofft.analyze(spectLength);
    let spectrumCopy = spectrum.splice(0,spectLength);
    // waveform = audiofft.waveform(waveLength);
    // waveLength = waveform.length;
    background(0);

    blocks.generateColumn(spectrumCopy)
    blocks.draw();
    blocks.moveBlocks();
    blocks.transitionBlocks();
}

function MusicBlock(xin, yin, siz, col) {
    this.size = siz;
    this.x = xin;
    this.y = yin;
    this.colour = col;

    this.draw = function() {
        fill(this.colour);
        rect(this.x - this.size/2, this.y - this.size/2, this.size, this.size);
    }

    this.move = function(moveX, moveY) {
        this.x = this.x + moveX;
        this.y = this.y + moveY;
    }
}

function MusicWriter(xin, yin, directionin, speedin, moveRatein, visLinesin, blockSizein, saveOld, threshin, offsetin) {
    // x and y indicate the line the blocks will spawn on
    this.x = xin;
    this.y = yin;
    this.speed = speedin;
    this.moveRate = moveRatein;
    this.visibleLines = visLinesin;
    this.blockSize = blockSizein;
    // direction is a lowercase string ["up" | "down" | "left" | "right"]
    // that indicates the direction that new music blocks appear from the 
    // old ones
    this.direction = directionin;
    this.outOfView = [];
    this.currentBlocks = [];
    this.frontBlocks = [];

    this.spawnDelay = 0;
    this.timeSinceSpawn = 0;

    this.save = saveOld;

    this.threshold = threshin;
    this.offset = offsetin;

    this.transitionBlocks = function() {
        // Move new blocks to current blocks
        if (this.frontBlocks.length != 0) {
            // this.currentBlocks.push(this.frontBlocks);
            let temp = [];
            this.currentBlocks.push(temp);
            for (let b = 0; b < this.frontBlocks.length; b++) {
                this.currentBlocks[this.currentBlocks.length-1].push(this.frontBlocks[b]);
            }
            this.frontBlocks.splice(0, this.frontBlocks.length);
        }

        // Remove blocks after they have passed from view
        if (this.currentBlocks.length >= this.visibleLines) {
            if (this.save) {
                this.outOfView.push(this.currentBlocks[0]);
            }
            this.currentBlocks.splice(0, 1);
        }
    }

    this.moveBlocks = function() {
        // Move the blocks
        if (frameCount % this.moveRate == 0) {
            switch (this.direction) {
                case "up":
                    for (let b = 0; b < this.frontBlocks.length; b++) {
                        this.frontBlocks[b].move(0, this.speed);
                    }
                    for (let c = 0; c < this.currentBlocks.length; c++) {
                        for (let b = 0; b < this.currentBlocks[c].length; b++) {
                            this.currentBlocks[c][b].move(0, this.speed);
                        }
                    }
                    break;
                case "down":
                    for (let b = 0; b < this.frontBlocks.length; b++) {
                        this.frontBlocks[b].move(0, -this.speed);
                    }
                    for (let c = 0; c < this.currentBlocks.length; c++) {
                        for (let b = 0; b < this.currentBlocks[c].length; b++) {
                            this.currentBlocks[c][b].move(0, -this.speed);
                        }
                    }
                    break;
                case "left":
                    for (let b = 0; b < this.frontBlocks.length; b++) {
                        this.frontBlocks[b].move(this.speed, 0);
                    }
                    for (let c = 0; c < this.currentBlocks.length; c++) {
                        for (let b = 0; b < this.currentBlocks[c].length; b++) {
                            this.currentBlocks[c][b].move(this.speed, 0);
                        }
                    }
                    break;
                case "right":
                    for (let b = 0; b < this.frontBlocks.length; b++) {
                        this.frontBlocks[b].move(-this.speed, 0);
                    }
                    for (let c = 0; c < this.currentBlocks.length; c++) {
                        for (let b = 0; b < this.currentBlocks[c].length; b++) {
                            this.currentBlocks[c][b].move(-this.speed, 0);
                        }
                    }
                    break;
            }
        }
    }

    this.draw = function() {
        push();
            colorMode(HSB);
            noStroke();
            for (let b = 0; b < this.frontBlocks.length; b++) {
                this.frontBlocks[b].draw();
            }
            for (let c = 0; c < this.currentBlocks.length; c++) {
                for (let b = 0; b < this.currentBlocks[c].length; b++) {
                    this.currentBlocks[c][b].draw();
                }
            }
        pop();
    }

    this.generateColumn = function(levels) {
        if (this.timeSinceSpawn >= this.spawnDelay) {
            this.timeSinceSpawn = 0;

            let newBlockSize = 0;
            if (this.blockSize != 0) {
                newBlockSize = this.blockSize;
            } else {
                if (this.direction == "up" || this.direction == "down") {
                    newBlockSize = (this.width / levels.length) / 2;
                } else {
                    newBlockSize = (this.height / levels.length) / 2;
                }
            }
            switch (this.direction) {
                case "up":
                case "down":
                    let xOff = width * 0.18;

                    for (let freq = 0; freq < levels.length; freq++) {
                        if (levels[freq] >= Math.max(this.threshold - (this.offset * freq), 20)) {
                            // generate musicBlock
                            let xCalc = xOff + (((width - (2 * xOff)) / (levels.length - 1)) * freq);
                            this.frontBlocks.push(new MusicBlock(xCalc, this.y, newBlockSize, [(((freq + 130) * 12) % 361), 60, 100]));
                        }
                    }
                    break;
                case "left":
                case "right":
                    let yOff = height * 0.15;

                    for (let freq = 0; freq < levels.length; freq++) {
                        if (levels[freq] >= Math.max(this.threshold - (this.offset * freq), 20)) {
                            // generate musicBlock
                            let yCalc = yOff + (((height - (2 * yOff))/(levels.length - 1)) * freq);
                            this.frontBlocks.push(new MusicBlock(this.x, yCalc, newBlockSize, [(((freq + 130) * 12) % 361), 60, 100]));
                        }
                    }
                    break;
            }

            if (this.spawnDelay == 0) {
                // Calculate spawn delay
                if (this.direction == "up" || this.direction == "down") {
                    this.spawnDelay = (this.moveRate * (newBlockSize / this.speed)) * 2;
                } else {
                    this.spawnDelay = (this.moveRate * (newBlockSize / this.speed)) * 2;
                }
            }
        } else {
            this.timeSinceSpawn = this.timeSinceSpawn + 1;
        }
    }
}


