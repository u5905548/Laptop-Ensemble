
ArrayList<Box> arr =  new ArrayList();


ArrayList<RotSquare> squares =  new ArrayList();


void setup() {
  size(1000,500, P3D);
  
  
  for (int i = 0; i < 30; i++) {
    arr.add(new Box(random(-100,100),random(-100,100),random(-100,100)));
  }
  
  for (int i = 0; i < 36; i++) {
    
    RotSquare r = new RotSquare(sin(i * TWO_PI / 36) * 100, cos(i * TWO_PI / 36) *100 ,tan(i * TWO_PI / 36) *100 );
    //r.setRotZ(i / 12);
    squares.add(r);
  }
}

float rotX = 0;
float rotY = 0;
float rotZ = 0;

float c = 0;

void draw() {
  background(0);
  translate(sketchWidth()/2, sketchHeight()/2, 0);
  
  rotateX(rotX);
  rotateY(rotY);
  rotateZ(rotZ);
  
  //for (int i = 0; i < arr.size(); i++) {
  //  arr.get(i).show();
  //}
  
  for (int i = 0; i < squares.size(); i++) {
    squares.get(i).show();
    //squares.get(i).update();
    //squares.get(i).setRotZ(i*c);
  }
  
  for (int i = 0; i < squares.size() / 2; i++) {
    beginShape();
    
    endShape(CLOSE);
  }
  
  c+=0.0005;
  //print( c + " val \n");
  
  //rotZ += 0.005;
  
}

void keyPressed() {
  print(keyCode + "\n");
  
  switch (keyCode) {
    case 88:  // X
      rotX += 0.2;
      break;
    case 90:  // Z
      rotX -= 0.2;
      break;
    case 65:  // A
      rotY += 0.2;
      break;
    case 83:  // S
      rotY -= 0.2;
      break;
    case 81:  // Q
      rotZ += 0.2;
      break;
    case 87:  // W
      rotZ -= 0.2;
      break;
    case 32: 
       
      rotX = 0;
      rotY = 0;
      rotZ = 0;
      break;
  }
}

class RotSquare {
  float x, y, z;
  PVector rotation;
  float[] points = new float[] {
    0, -20, -20,
    0,-20, 20,
    0, 20, 20,
    0, 20,-20
    
  };
    
  public RotSquare (float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
    
    this.rotation = new PVector(0f,0f,0f);
    
  }
  
  private void update() {
    this.rotation.x += 0.02f;
  }
  
  private void show() {
    pushStyle();
    stroke(255);
    noFill();
    pushMatrix();
    translate(this.x,this.y,this.z);
      pushMatrix();
        rotateZ(this.rotation.z);
        
        
        pushMatrix();
          
          rotateX(this.rotation.x);
          beginShape();
            vertex(0,-20,-20);
            vertex(0,-20, 20);
            vertex(0, 20, 20);
            vertex(0, 20,-20);
          endShape(CLOSE);
          
        popMatrix();
      popMatrix();
      popMatrix();
    popStyle();
  }
  
  private void setRotZ(float r) {
    this.rotation.z = r;
  }

}

class Box {
  float x, y, z;
  
  public Box (float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
    
  }
  
  private void show() {
  
    pushStyle();
      pushMatrix();
        translate(this.x,this.y,this.z);
        box(20);
      popMatrix();
      
    popStyle();
  
  }
}