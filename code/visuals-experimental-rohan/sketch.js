// Rohan Proctor 2018

var audio;
var audiofft;
var testSongInput;

var bass;
var lowMid;
var mid;
var highMid;
var treble;
var clap;

var bgnum = [230,0,0];
var spectrum;

var ringCount = 6;
var bins = 24;
var amplitude = 0.2;
var rotation = 0;
var rotating = false;
var rotSpeed = 0;

var strokeHue = 40;
var strokeSat = 200;
var strokeBri = 100;
var strokeThick = 3;

var fillHue = 20;
var fillSat = 200;
var fillBri = 120;

var poly;
var presets;



// function preload() {
//     testSongInput = loadSound('song.mp3');
// }

var goingUp = true;
var steps = 0;
var burstGo = false;

var counter = 0;

function ringsUpDown() {

    if (burstGo) {
        if (counter > 2) {
            if (goingUp) {
                ringCount++;
                steps++;
            } else {
                ringCount--;
                steps--;
                if (steps == 0)  {
                    burstGo = false;
                }
            }
            counter = 0;
        }
        counter++;
    }

    if (steps > 8) {
        goingUp = false;
    }
}


function setup() {
    createCanvas(windowWidth, windowHeight, P2D);
    colorMode(HSB);
    
    audio = new p5.AudioIn();
    // audio.setSource(0);

    audio.start();
    audio.amp(0.5); 

    // My computer microphone only senses up to around 24 bands of the spectrum
    // It will be different for audio input though, max for loading songs to test with was around 36

    // Preset values [fillHue, Sat, Bright, ringCount, bins, amplitude, rotation, rotSpeed, rotating?, 
    //  strokeHue, StrokeSat, StrokeBright, StrokeWeight]
    presets = [
        [118,  50,  60,  6, 24,   0.2,  0,     0, false, 118, 180, 200, 2], // default
        [  0, 200, 150,  5,  3,     0, PI,  0.02,  true,   0, 255,  40, 4], // red triangle 
        [ 20, 200, 120,  8, 26,   0.4, PI,     0, false,  40, 200, 100, 6], // 
        [202,  85, 194,  5, 15, -1.18,  0, 0.024,  true, 226, 212,  65, 5],
        [144, 196, 177, 9,34,-0.43,0,-0.013,true,89,251,28,12],
        [5.030,58.386,157.8707,6,8,1.4,3.6980,0.020129942122811897,true,44.09388512527782,14.89949686259042,1.264811372223379,6.1427985389141995],
        [192.13146424904085,40.766220536570735,22.514947390693987,7,19,1.2753141848097158,5.142805279496953,-0.025503793124729234,true,14.770749138735571,105.63094205289671,88.21475946274192,19.182633483385317],
        [152.73930063975408,138.9578081956279,60.23493456700825,6,16,0.11164975010068713,6.2651156947726045,0.0005593903476754612,true,91.23684745163159,88.66875865459414,17.384144752011906,9.324449171769968],
    ];

    
    // testSongInput.loop();
    
    // testSongInput.setVolume(0.5);

    audiofft = new p5.FFT(0.7, 64); // Using 64 bin spectrum
    audiofft.setInput(audio);
    
    poly = new PolyLine();
}

var sceneFrameCount = 0;

var centreX = 0;
var centerY = 0;

var inc = 0;
var noiseOffset = 0;

var moveInCircle = false;
var noiseMove = false;

var centreX = 0;
var centreY = 0;

var noiseMoveScale = 20;

function draw() {
    background(0);
    
    mid = audiofft.getEnergy('mid');

    if (rotating) rotation += rotSpeed;

    spectrum = audiofft.analyze();

    // Lower bass weighting
    for (let i = 0; i < 5; i++) {
        spectrum[i] *= (0.75 + 0.05*i);
    }

    inc += 0.01;

    // moving around centre point for a cool 3D effect
    if (moveInCircle) {
        centreX = 20 * cos(inc);
        centreY = 20 * sin(inc);
    } else if (noiseMove) {
        centreX = map( noise(noiseOffset),     0, 1, -noiseMoveScale, noiseMoveScale );
        centreY = map( noise(noiseOffset + 2), 0, 1, -noiseMoveScale, noiseMoveScale );

        noiseOffset += map(mid , 0, 255, 0, 0.02);
    }
    
	for(let i = 1; i < ringCount; i++) {

        push();
        
            fill(fillHue + i * 5, fillSat, fillBri);
            stroke(strokeHue, strokeSat, strokeBri);
            strokeWeight(strokeThick);
                
            translate(width/2 + (centreX * i),height/2 + (centreY * i));
            rotate(rotation);

            // draw shapes from outside in
            poly.show((ringCount - i) * (20+i));
 
        pop();
    }

    if (burstGo) {
        ringsUpDown();
    }

    if (sceneFrameCount < (120 * 60)) {
        if (sceneFrameCount % 600 == 0) {
            if (random() < 0.3) {
                var o = floor(random(0,presets.length));
    
                if (!(o >= 0 && o <= presets.length - 1)) o = 3;
                
                newPreset(o);
            } else {
                randomPoly();
            }
        }
    } else {
        if (sceneFrameCount % 300 == 0) {
            if (random() < 0.3) {
                var o = floor(random(0,presets.length));
                if ( !o >= 0 || !o <= presets.length - 1) o = 3;
                
                newPreset(o);
            } else {
                randomPoly();
            }
        }
    }

    
    if (sceneFrameCount % 90 == 0) {
        var r = random();
        if(r >= 0 && r < 0.33) {
            if (ringCount < 11) ringCount++;
        } else if (r >= 0.33 && r < 0.66) {
            if (ringCount >= 4) ringCount--;
        }
    }

    if (sceneFrameCount % (600*4) == 0) {
        if (sceneFrameCount != 0) burst();
    }

    // Control flow reliant on Scene frame count, brings in more variance and intensity over time

    if ((sceneFrameCount > (60 * 30) && sceneFrameCount < (60 * (30 + 45)))
        || (sceneFrameCount > (60 * (350)) && sceneFrameCount < (60 * (380))) ) {
        centreX = 40;
        centreY = 40;
    }

    if ((sceneFrameCount > (60 * (90)) && sceneFrameCount < (60 * (30 + 60 + 45)))
        || (sceneFrameCount > (60 * (390)) && sceneFrameCount < (60 * (440))) ) {
        moveInCircle = true;
    } else {
        moveInCircle = false;
    } 

    if ((sceneFrameCount > (60 * (90 + 60)) && sceneFrameCount < (60 * (150 + 45)))
        || (sceneFrameCount > (60 * (450)) && sceneFrameCount < (60 * (500))) ) {
        noiseMove = true; 
    } else {
        noiseMove = false;
    }

    if (( sceneFrameCount > (60 * (195)) && sceneFrameCount < (60 * (195 + 30)))
        || (sceneFrameCount > (60 * (510)) && sceneFrameCount < (60 * (540))) )  {
        centreX = -40;
        centreY = -20;
    } 

    if ((sceneFrameCount > (60 * (195 + 60)) && sceneFrameCount > (60 * (300)))
        || (sceneFrameCount > (60 * (540)) && sceneFrameCount < (60 * (600))) ) {
        noiseMoveScale = 50;
        noiseMove = true; 
    } else {
        noiseMove = false;
    }

    if (( sceneFrameCount > (60 * (300)) && sceneFrameCount > (60 * (350))) 
        || (sceneFrameCount > (60 * (610))) )  {
            if (sceneFrameCount % 100 == 0) {
                centreX = random(-40, 40);
                centreY = random(-40, 40);
            }
        
    }

    sceneFrameCount++;    

    // Un-comment for FPS counter
    // push();
    // var fps = frameRate();
    // fill(255);
    // stroke(0);
    // text("FPS: " + fps.toFixed(2), 10, height - 10);
    // pop();
}

function burst() {
    burstGo = true;
    counter = 0;
    goingUp = true;
}

function keyPressed() {

    switch (keyCode) {
        case LEFT_ARROW:
            bins++; 
            break;
        case RIGHT_ARROW:
            bins--;
            break;
        case DOWN_ARROW:
            ringCount--;
            break;
        case UP_ARROW:
            ringCount++;
            break;    
        case 65:
            amplitude+=0.2; // A
            break;  
        case 83:
            amplitude-=0.2; // S
            break;  
        case 90:
            newPreset(0); // Z
        break;  
        case 88:
            newPreset(1); // X
        break;
        case 67:
            newPreset(3); // C
        break;
        case 70:          // F
            burstGo = true;
            counter = 0;
            goingUp = true;
        break;
        case 71:          // G
            randomPoly();
        break;
        default:
            break;
    }
    
}

function PolyLine() {

    this.show = function(rad) {
		push();
            beginShape();
                for (let i = 0; i < bins; i++) {

                    nVal = map(spectrum[i], 0, 255, amplitude, 3); // map noise value to match the amplitude
                    
                    x = cos(i * TWO_PI/bins) * nVal * rad;
                    y = sin(i * TWO_PI/bins) * nVal * rad;

                    vertex(x, y);
                }
            endShape(CLOSE);
        pop();
        
    }
}

function newPreset(index) {
    
    fillHue     = presets[index][0];
    fillSat     = presets[index][1];
    fillBri     = presets[index][2];
    ringCount   = presets[index][3];
    bins        = presets[index][4];
    amplitude   = presets[index][5];
    rotation    = presets[index][6];
    rotSpeed    = presets[index][7];
    rotating    = presets[index][8];
    strokeHue   = presets[index][9];
    strokeSat   = presets[index][10];
    strokeBri   = presets[index][11];
    strokeThick = presets[index][12];
    
}

function randomPoly() {
    fillHue     = random(0,255);
    fillSat     = random(0,255);
    fillBri     = random(0,240);
    ringCount   = floor(random(4,11));
    bins        = floor(random(3,38));
    amplitude   = random(-0.2,1.5);
    rotation    = random(0, TWO_PI);
    rotSpeed    = random(-0.025,0.025);

    if (random() > 0.8) {
        rotating = false 
    } else {
        rotating = true;
    }
    strokeHue   = random(0,255);
    strokeSat   = random(0,255);
    strokeBri   = random(0,255);
    strokeThick = random(1,20);

    // console.log("------------------------");
    // console.log("HSB:" + fillHue + " " + fillSat + " " + fillBri);
    // console.log("RCount: " + ringCount);
    // console.log("Bins: " + bins);
    // console.log("Amp: " + amplitude);
    // console.log("Rotation: " + rotation);
    // console.log("RSpeed: " + rotSpeed);
    // console.log("rotating " + rotating);
    // console.log("Stroke HSB " + strokeHue + " " + strokeSat + " " + strokeBri);
    // console.log("thick " + strokeThick);

    // So can copy paste presets into code if a good one is found

    // Preset values [fillHue, Sat, Bright, ringCount, bins, amplitude, rotation, rotSpeed, rotating?, 
    //  strokeHue, StrokeSat, StrokeBright, StrokeWeight]

    // console.log("["+fillHue+","+fillSat+","+fillBri+","+ringCount+","+bins+","+amplitude+","+rotation+","+rotSpeed
    //     +","+rotating+","+strokeHue+","+strokeSat+","+strokeBri+","+strokeThick+"]");

}
